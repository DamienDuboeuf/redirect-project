# symfony-job-interview-test

We had a great idea of business: we should do the same thing than bit.ly! And here we are

# Install notes

```
docker-compose up -d
docker-compose exec app composer install
```

#### Import CA in chrome

 - Open google-chrome
 - Go "Confidentialité et sécurité" -> "Sécurité" -> "Gérer les certificats" -> "Autorité" -> "importer" 
 - Load `./config/nginx/ssl/rootCA.pem`

# Database structure

```
docker-compose exec app bin/console doctrine:migrations:migrate -n
```

# Fixtures

```
docker-compose exec app bin/console doctrine:fixtures:load -n
```

# How to test


Go to https://localhost/redirect/some_token

