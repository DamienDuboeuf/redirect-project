<?php
namespace App\DataFixtures\ORM;

use App\Entity\Uri;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UriFixture extends Fixture
{
	public function load(ObjectManager $manager)
	{
		$uri = new Uri();
		$uri->setUrl('https://www.youtube.com/watch?v=dQw4w9WgXcQ');
		$uri->setToken('some_token');
		$manager->persist($uri);
		
		for ($i = 0; $i < 100; $i++) {
			$uri = new Uri();
			$uri->setUrl('https://picsum.photos/'.(200 + $i));
			$uri->setToken('some_token'.$i);
			$manager->persist($uri);
		}
		
		$manager->flush();
	}
}
