<?php
namespace App\Controller;

use App\Entity\Uri;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class RedirectController
{
	
	// Utilisation de l'annotation car le dossier controller est déjà déclaré dans services.yml
	/**
	 * @Route("/redirect/{token}") // token est unique on peux donc utilisé un param converter (renvoie une 404 si n'existe pas)
	 */
	public function index(Uri $uri, EntityManagerInterface $em)
	{
		$uri->moreUse();
		$em->flush();
		return new RedirectResponse($uri->getUrl(), 301);
	}
	
}
