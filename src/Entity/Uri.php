<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Uri
{
    /**
     * @ORM\Column(type="integer", length=25, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * 
     * @var string
     */
    private $token;
    
    /**
     * @ORM\Column(type="text")
     * 
     * @var string
     */
    private $url;
    
    /**
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * 
     * @var integer
     */
    private $timesUsed = 0;
    
    /////////////
    // Getters //
    /////////////
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function getToken(): string
    {
        return $this->token;
    }
    
    public function getUrl(): string
    {
        return $this->url;
    }
    
    public function getTimesUsed(): int
    {
        return $this->timesUsed;
    }
    
    /////////////
    // Setters //
    /////////////
    
    public function setToken(string $token)
    {
        $this->token = $token;
        return $this;
    }
    
    public function setUrl(string $url)
    {
        $this->url = $url;
        return $this;
    }
    
    ////////////
    // Others //
    ////////////
    
    public function moreUse()
    {
        $this->timesUsed++;
        return $this;
    }
}
